let jsonData = require("../events.json");
const fs = require("fs");

const analize = async () => {
  const events = {};
  jsonData.forEach((event) => {
    if (events[event.event_type]) {
      delete event.id,
      delete event.timestamp,
      delete event.care_recipient_id,

      events[event.event_type] = [event, ...events[event.event_type]];
    } else {
      events[event.event_type] = [];
    }
  });

  Object.keys(events).forEach((eventType) => {
    fs.writeFileSync(`./dataAnalisys/eventTypes/${eventType}.json`, JSON.stringify(events[eventType]));
  });

};

analize();
