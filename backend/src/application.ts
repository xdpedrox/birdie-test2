import * as express from "express";
import { pingController } from "./controllers/ping";
import { eventsController } from "./controllers/events";

const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const swaggerDocument = YAML.load(
  "/Users/pedro/Documents/birdie-playgroud/birdie-test/backend/src/openapi/index.yaml"
);

const app = express();

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(pingController);
app.use(eventsController);

export default app;
