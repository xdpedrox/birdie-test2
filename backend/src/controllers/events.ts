import * as express from "express";
import { PrismaClient } from "@prisma/client";
import * as R from "ramda";
export const eventsController = express.Router();

const prisma = new PrismaClient();

interface whereOptions {
  event_type?: string;
  care_recipient_id?: string;
  alert_id?: string;
  task_instance_id?: string;
  visit_id?: string;
  caregiver_id?: string;
  rejected_event_id?: string;
  observation_event_id?: string;
}

interface rangeOptions {
  start_date?: string;
  end_date?: string;
}

eventsController.get("/events", async (req: express.Request, res: express.Response) => {
  const params: {
    page?: number;
    per_page?: number;
    order?: "desc" | "asc";
    where?: whereOptions;
    dates?: rangeOptions;
  } = req.query;

  const { page = 1, per_page = 25, where } = params;

  const rowCount = await prisma.events.count();
  const events = await prisma.events.findMany({
    // Pagination
    skip: ((+page - 1) * +per_page),
    take: +per_page,
    // Filter
    where: where,
    // Order
    orderBy: {
      timestamp: "desc",
    },
  });

  const eventsPayload = events.map((event) => event.payload);

  res.status(200).json({
    data: eventsPayload,
    totalPages: Math.ceil(rowCount / +per_page),
    fetchedRows: events.length,
    page: +page,
  });
});
