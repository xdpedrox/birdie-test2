import { BaseEvent } from "./lib/Base";

export interface BasicEvent extends BaseEvent {
  event_type: 'medication_schedule_updated' | 'regular_medication_partially_taken' | 'toilet_visit_recorded';
}