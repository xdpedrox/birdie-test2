import { BaseEvent } from "./lib/Base";
import { CaregiverVisit } from "./lib/CaregiverVisit";

export interface TaskCompletedOrReversed extends BaseEvent, CaregiverVisit {
  event_type: 'task_completed' | 'task_completion_reverted';
  task_instance_id: string;
  task_schedule_id: string;
  task_definition_id: string;
  task_schedule_note: string;
  task_definition_description: string;
}