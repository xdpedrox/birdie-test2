import { BaseEvent } from "./lib/Base";
import { CaregiverVisit } from "./lib/CaregiverVisit";

interface Observation extends BaseEvent {
  note: string;
}

interface Media {
  src: string;
  type: string;
}

enum Mood {
  Happy = "happy",
  Okay = "okay",
  Sad = "sad",
}

export interface CatheterObservation extends Observation, CaregiverVisit {
  volume_ml: number;
  event_type: "catheter_observation";
}

export interface GeneralObservation extends Observation, CaregiverVisit {
  event_type: "general_observation";
  media: Media[];
}

export interface MentalOrPhysicalHealthObservation
  extends Observation,
    CaregiverVisit {
  event_type: "mental_health_observation" | "physical_health_observation";
}

export interface MoodObservation extends BaseEvent, CaregiverVisit {
  mood: Mood;
  event_type: "mood_observation";
  note?: string;
}
