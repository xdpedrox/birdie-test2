import { BaseEvent } from "./lib/Base";
import { CaregiverVisit } from "./lib/CaregiverVisit";

export interface FoodIntakeObservation extends BaseEvent, CaregiverVisit {
  meal: Meal;
  note: string;
  event_type: "food_intake_observation";
}

export interface FluidIntakeObservation extends BaseEvent, CaregiverVisit {
  fluid: Fluid;
  observed: boolean;
  event_type: "fluid_intake_observation";
  consumed_volume_ml: number;
}

enum Meal {
  Meal = "meal",
  Snack = "snack",
}

enum Fluid {
  Alcoholic = "alcoholic",
  Caffeinated = "caffeinated",
  Regular = "regular",
}
