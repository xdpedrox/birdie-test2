import { BaseEvent } from "./lib/Base";

interface Alert extends BaseEvent {
  alert_id: string;
}

enum AlertSeverity {
  High = "HIGH",
  Low = "LOW",
  Medium = "MEDIUM",
}

export interface AlertQualified extends Alert {
  event_type: "alert_qualified";
  caregiver_id: string;
  alert_severity: AlertSeverity;
}

export interface AlertRaised {
  event_type: "alert_raised";
  caregiver_id?: string;
  observation_event_id: string;
}
