import { BaseEvent } from "./lib/Base";

interface Schedule extends BaseEvent {
  rrule: string;
  note: string;
}

export interface TaskScheduleCreated extends Schedule {
  event_type: "task_schedule_created";
  caregiver_id: string;
  task_schedule_id: string;
  task_definition_id: string;
}

export interface MedicationScheduleCreated extends Schedule {
  type: string;
  user_id: string;
  dose_size: string;
  event_type: "medication_schedule_created";
  medical_product_id: string;
  medication_schedule_id: string;
}
