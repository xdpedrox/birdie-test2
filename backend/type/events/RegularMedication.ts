import { BaseEvent } from "./lib/Base";
import { CaregiverVisit } from "./lib/CaregiverVisit";

interface RegularMedication extends BaseEvent, CaregiverVisit {
  task_instance_id: string;
}

enum MedicationFailureReason {
  MedicationError = "MEDICATION_ERROR",
  NauseaAndVomiting = "NAUSEA_AND_VOMITING",
  Other = "OTHER",
  Refused = "REFUSED",
}

enum MedicationType {
  Prn = "PRN",
  Scheduled = "SCHEDULED",
  Blister = "BLISTER",
}

export interface RegularMedicationTaken extends RegularMedication {
  note?: string;
  event_type: "regular_medication_taken";
  medication_failure_reason?: null;
  medication_type?: MedicationType;
}

export interface RegularMedicationNotTaken extends RegularMedication {
  note: string;
  event_type: "regular_medication_not_taken";
  medication_failure_reason: MedicationFailureReason;
  medication_type?: MedicationType;
}

export interface RegularMedicationMaybeTaken extends RegularMedication {
  note: string;
  event_type: "regular_medication_maybe_taken";
  medication_failure_reason: MedicationFailureReason;
}

export interface NoMedicationObservationReceived extends BaseEvent {
  event_type: "no_medication_observation_received";
  medication_type?: MedicationType;
  task_instance_id: string;
  expected_dose_timestamp?: string;
}
