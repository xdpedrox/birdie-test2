import { BaseEvent } from "./lib/Base";
import { CaregiverVisit } from "./lib/CaregiverVisit";

export interface VisitEvent extends BaseEvent, CaregiverVisit {
  event_type: 'check_in' | 'check_out' | 'visit_cancelled' | 'visit_completed';
}

