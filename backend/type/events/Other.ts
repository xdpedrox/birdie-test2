import { BaseEvent } from "./lib/Base";
import { CaregiverVisit } from "./lib/CaregiverVisit";

export interface ConcernRaised extends BaseEvent, CaregiverVisit {
  note: string;
  severity: string;
  visit_id: string;
  event_type: 'concern_raised';
  navigation: {
    params: {
      recipientId: string;
    };
    [key: string]: any;
  };
  screenProps: {
    role: string;
  };
  caregiver_id: string;
  observations: any[];
}

export interface IncontinencePadObservation {
  visit_id: string;
  event_type: "incontinence_pad_observation";
  navigation: {
    params: {
      recipientId: string;
    };
    [key: string]: any;
  };
  screenProps: {
    careRecipientId: string;
  };
  caregiver_id: string;
  observations: any[];
  pad_condition: PadCondition;
  note?: string;
}

export enum PadCondition {
  Dry = "dry",
  Soiled = "soiled",
  Wet = "wet",
}
