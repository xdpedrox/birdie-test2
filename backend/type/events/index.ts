import { AlertQualified, AlertRaised } from "./Alert";
import { BasicEvent } from "./BasicEvent";
import {
  FoodIntakeObservation,
  FluidIntakeObservation,
} from "./IntakeObservation";
import {
  CatheterObservation,
  GeneralObservation,
  MentalOrPhysicalHealthObservation,
  MoodObservation,
} from "./Observations";
import { ConcernRaised, IncontinencePadObservation } from "./Other";
import {
  RegularMedicationTaken,
  RegularMedicationNotTaken,
  RegularMedicationMaybeTaken,
  NoMedicationObservationReceived,
} from "./RegularMedication";
import { TaskScheduleCreated, MedicationScheduleCreated } from "./Schedule";
import { TaskCompletedOrReversed } from "./TaskCompleted";
import { VisitEvent } from "./VisitEvent";

export type Event =
  | AlertQualified // alert_qualified
  | AlertRaised // alert_raised
  | BasicEvent // medication_schedule_updated, regular_medication_partially_taken, toilet_visit_recorded
  | FoodIntakeObservation // food_intake_observation
  | FluidIntakeObservation // fluid_intake_observation
  | CatheterObservation // catheter_observation
  | GeneralObservation // general_observation
  | MentalOrPhysicalHealthObservation // mental_health_observation, physical_health_observation
  | MoodObservation // mood_observation
  | ConcernRaised // concern_raised
  | IncontinencePadObservation // incontinence_pad_observation
  | RegularMedicationTaken // regular_medication_taken
  | RegularMedicationNotTaken // regular_medication_not_taken
  | RegularMedicationMaybeTaken // regular_medication_maybe_taken
  | NoMedicationObservationReceived // no_medication_observation_received
  | TaskScheduleCreated // task_schedule_created
  | MedicationScheduleCreated // medication_schedule_created
  | TaskCompletedOrReversed // task_completed, task_completion_reverted
  | VisitEvent // check_in, check_out, visit_cancelled, visit_completed
